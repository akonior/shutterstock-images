package pl.itako.rxjavaexample;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import pl.itako.rxjavaexample.dagger.AppComponent;
import pl.itako.rxjavaexample.dagger.DaggerAppComponent;
import pl.itako.rxjavaexample.network.ShutterstockImagesPager;
import pl.itako.rxjavaexample.network.ShutterstockService;
import rx.schedulers.Schedulers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ShutterstockServiceIntegrationTest {

    private ShutterstockService service;
    private ShutterstockImagesPager pager;

    @Before
    public void before() {
        AppComponent component = DaggerAppComponent.builder().build();
        service = component.service();
        pager = component.pager();
    }

    @Test
    public void testServiceGetPage() throws Exception {
        service.getPage(1).subscribe(page -> {
            assertEquals("Incorrect number of returned items", 20, page.data.size());
        }, throwable -> {
            fail(throwable.getMessage());
        });
    }

    @Test
    public synchronized void testPager() throws Exception {
        final int[] numberOfInvocations = {0};
        pager.getImages()
                .subscribeOn(Schedulers.immediate())
                .observeOn(Schedulers.immediate())
                .take(20)
                .timeout(4, TimeUnit.SECONDS)
                .subscribe(image -> {
                    numberOfInvocations[0]++;
                }, throwable -> {
                    synchronized (ShutterstockServiceIntegrationTest.this) {
                        ShutterstockServiceIntegrationTest.this.notifyAll();
                    }
                }, () -> {
                    synchronized (ShutterstockServiceIntegrationTest.this) {
                        ShutterstockServiceIntegrationTest.this.notifyAll();
                    }
                });

        pager.next();
        wait();
        assertEquals(20, numberOfInvocations[0]);
    }
}