package pl.itako.rxjavaexample.dagger;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.itako.rxjavaexample.network.ShutterstockImagesPager;
import pl.itako.rxjavaexample.network.ShutterstockService;
import retrofit2.Retrofit;

@Module
public class AppModule {

    @Provides
    @Singleton
    ShutterstockService provideShutterstockService(Retrofit retrofit) {
        return retrofit.create(ShutterstockService.class);
    }

    @Provides
    ShutterstockImagesPager provideShutterstockImagesPager(ShutterstockService shutterstockService) {
        return new ShutterstockImagesPager(shutterstockService);
    }
}
