package pl.itako.rxjavaexample;

import android.app.Application;

import pl.itako.rxjavaexample.dagger.AppComponent;
import pl.itako.rxjavaexample.dagger.DaggerAppComponent;

public class ShutterstockImagesApp extends Application {

    private static AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerAppComponent.builder().build();
    }

    public static AppComponent getComponent() {
        return component;
    }

    public static void setComponent(AppComponent component) {
        ShutterstockImagesApp.component = component;
    }
}
