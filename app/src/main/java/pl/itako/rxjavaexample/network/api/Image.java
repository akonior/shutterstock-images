package pl.itako.rxjavaexample.network.api;

public class Image {
    public final String id;
    public final Assets assets;

    public Image(String id, Assets assets) {
        this.id = id;
        this.assets = assets;
    }
}
