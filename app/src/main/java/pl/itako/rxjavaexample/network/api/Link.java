package pl.itako.rxjavaexample.network.api;

public class Link {
    public final String url;

    public Link(String url) {
        this.url = url;
    }
}
