package pl.itako.rxjavaexample.network;

import pl.itako.rxjavaexample.network.api.Page;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import rx.Observable;

public interface ShutterstockService {

    int ITEMS_PER_PAGE = 20;

    @Headers("Authorization: Basic ZTBmZjYyMjljNjBhMjU4ZjM5M2Q6ZTcxYjRjYmM3YzA3M2NiZDhlZmEwOTZjM2E5ZTU4MTc5MDQ1NWU4Ng==")
    @GET("v2/images/search?query=nature&per_page=" + ITEMS_PER_PAGE)
    Observable<Page> getPage(@Query("page") int page);
}
