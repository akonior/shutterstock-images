package pl.itako.rxjavaexample.network.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Page {
    public final int page;
    public final @SerializedName("per_page") int perPage;
    public final List<Image> data;

    public Page(int page, int perPage, List<Image> data) {
        this.page = page;
        this.perPage = perPage;
        this.data = data;
    }
}
