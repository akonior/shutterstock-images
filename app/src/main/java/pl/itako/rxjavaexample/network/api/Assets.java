package pl.itako.rxjavaexample.network.api;

import com.google.gson.annotations.SerializedName;

public class Assets {
    public final Link preview;
    public final @SerializedName("small_thumb") Link smallThumb;
    public final @SerializedName("large_thumb") Link largeThumb;

    public Assets(Link preview, Link smallThumb, Link largeThumb) {
        this.preview = preview;
        this.smallThumb = smallThumb;
        this.largeThumb = largeThumb;
    }
}
