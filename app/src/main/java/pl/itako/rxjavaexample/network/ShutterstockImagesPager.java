package pl.itako.rxjavaexample.network;

import pl.itako.rxjavaexample.network.api.Image;
import rx.Observable;
import rx.Subscription;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

public class ShutterstockImagesPager {

    private final ShutterstockService service;
    private PublishSubject<Observable<Image>> pages = PublishSubject.create();

    int pageIndex = 1;

    public ShutterstockImagesPager(ShutterstockService service) {
        this.service = service;
    }

    public Observable<Image> getImages() {
        return Observable.<Image>create(subscriber -> {
            Subscription subscription = Observable.switchOnNext(pages).subscribe(subscriber);
            subscriber.add(subscription);
        }).subscribeOn(Schedulers.io());
    }

    public void next() {
        Observable<Image> images = service.getPage(pageIndex++).retry(5)
                .subscribeOn(Schedulers.io())
                .flatMap(page -> Observable.from(page.data));
        pages.onNext(images);
    }
}
