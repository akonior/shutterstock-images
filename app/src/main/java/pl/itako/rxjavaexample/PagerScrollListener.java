package pl.itako.rxjavaexample;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class PagerScrollListener extends RecyclerView.OnScrollListener {

    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 12;
    int firstVisibleItem, visibleItemCount, totalItemCount;

    private LinearLayoutManager mLinearLayoutManager;
    private final PagerScrollListenerNextPage listenerNextPage;

    public PagerScrollListener(LinearLayoutManager linearLayoutManager, PagerScrollListenerNextPage listenerNextPage) {
        this.mLinearLayoutManager = linearLayoutManager;
        this.listenerNextPage = listenerNextPage;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        visibleItemCount = recyclerView.getChildCount();
        totalItemCount = mLinearLayoutManager.getItemCount();
        firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();

        if (loading && totalItemCount > previousTotal) {
            loading = false;
            previousTotal = totalItemCount;
        }
        if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
            listenerNextPage.loadNextPage();
            loading = true;
        }
    }

    public interface PagerScrollListenerNextPage {
        void loadNextPage();
    }
}