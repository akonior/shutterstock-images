package pl.itako.rxjavaexample.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.itako.rxjavaexample.R;
import pl.itako.rxjavaexample.network.api.Image;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static com.squareup.picasso.Picasso.Priority.LOW;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ViewHolder> {

    private static final String TAG = ImagesAdapter.class.getSimpleName();

    private final List<Image> images = new ArrayList<>();
    private final Context context;
    private final Subscription subscription;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.image) ImageView image;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public ImagesAdapter(Context context, Observable<Image> images) {
        this.context = context;
        setHasStableIds(true);
        subscription = images.observeOn(AndroidSchedulers.mainThread()).subscribe(image -> {
            this.images.add(image);
            notifyItemInserted(this.images.size() - 1);
            Picasso.with(context).load(image.assets.preview.url).priority(LOW).fetch();
        }, throwable -> {
            Log.e(TAG, "Error: ", throwable);
            throwable.printStackTrace();
        });
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_row, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String url = images.get(position).assets.preview.url;
        Picasso.with(context).load(url).placeholder(null).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    @Override public long getItemId(int position) {
        return position;
    }

    public void unsubscribe() {
        Log.d(TAG, "unsubscribe()");
        subscription.unsubscribe();
    }
}
