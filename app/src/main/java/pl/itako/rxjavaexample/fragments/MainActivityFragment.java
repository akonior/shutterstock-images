package pl.itako.rxjavaexample.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.itako.rxjavaexample.PagerScrollListener;
import pl.itako.rxjavaexample.R;
import pl.itako.rxjavaexample.network.ShutterstockImagesPager;
import pl.itako.rxjavaexample.view.adapters.ImagesAdapter;

import static pl.itako.rxjavaexample.ShutterstockImagesApp.getComponent;

public class MainActivityFragment extends Fragment {

    @Bind(R.id.recyclerView) RecyclerView recyclerView;
    @Inject ShutterstockImagesPager pager;

    private ImagesAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);
        getComponent().inject(this);

        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addOnScrollListener(new PagerScrollListener(layoutManager, () -> pager.next()));

        adapter = new ImagesAdapter(getActivity(), pager.getImages());
        recyclerView.setAdapter(adapter);

        pager.next();
        return view;
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        adapter.unsubscribe();
    }
}
