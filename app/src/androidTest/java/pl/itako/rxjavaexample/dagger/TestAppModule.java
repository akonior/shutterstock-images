package pl.itako.rxjavaexample.dagger;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.itako.rxjavaexample.network.ShutterstockImagesPager;
import pl.itako.rxjavaexample.network.ShutterstockService;
import pl.itako.rxjavaexample.util.IdlingApiServiceWrapper;
import retrofit2.Retrofit;

@Module
public class TestAppModule {

    @Provides
    @Singleton
    ShutterstockService provideShutterstockService(Retrofit retrofit) {
        ShutterstockService service = retrofit.create(ShutterstockService.class);
        return new IdlingApiServiceWrapper(service);
    }

    @Provides
    ShutterstockImagesPager provideShutterstockImagesPager(ShutterstockService shutterstockService) {
        return new ShutterstockImagesPager(shutterstockService);
    }
}
