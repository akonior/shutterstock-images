package pl.itako.rxjavaexample.dagger;

import javax.inject.Singleton;

import dagger.Component;
import pl.itako.rxjavaexample.fragments.MainActivityFragment;
import pl.itako.rxjavaexample.network.ShutterstockImagesPager;
import pl.itako.rxjavaexample.network.ShutterstockService;

@Singleton
@Component(modules = {TestAppModule.class, NetModule.class})
public interface TestAppComponent extends AppComponent {
    ShutterstockService service();

    ShutterstockImagesPager pager();

    void inject(MainActivityFragment mainActivityFragment);
}

