package pl.itako.rxjavaexample;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import pl.itako.rxjavaexample.activities.MainActivity;
import pl.itako.rxjavaexample.dagger.DaggerTestAppComponent;
import pl.itako.rxjavaexample.dagger.TestAppComponent;
import pl.itako.rxjavaexample.util.IdlingApiServiceWrapper;
import pl.itako.rxjavaexample.util.RecyclerViewAssertions;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityTest {

    @Rule
    public ActivityTestRule mActivityRule = new ActivityTestRule(MainActivity.class) {
        @Override
        protected void beforeActivityLaunched() {
            super.beforeActivityLaunched();
            TestAppComponent component = DaggerTestAppComponent.builder().build();
            ShutterstockImagesApp.setComponent(component);
            Espresso.registerIdlingResources(((IdlingApiServiceWrapper) component.service()));
        }
    };

    @Test
    public void testMainActivity() throws InterruptedException {
        Espresso.onView(ViewMatchers.withId(R.id.recyclerView)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
        Espresso.onView(ViewMatchers.withId(R.id.recyclerView)).check(RecyclerViewAssertions.hasItemsCount(20));
    }
}
