package pl.itako.rxjavaexample.util;

import android.support.test.espresso.ViewAssertion;
import android.support.v7.widget.RecyclerView;

import org.hamcrest.CoreMatchers;

import static org.hamcrest.MatcherAssert.assertThat;

public class RecyclerViewAssertions {

    public static ViewAssertion hasItemsCount(final int count) {
        return (view, e) -> {
            if (!(view instanceof RecyclerView)) {
                throw e;
            }
            RecyclerView rv = (RecyclerView) view;
            assertThat(rv.getAdapter().getItemCount(), CoreMatchers.equalTo(count));
        };
    }
}
