package pl.itako.rxjavaexample.util;

import android.support.test.espresso.IdlingResource;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import pl.itako.rxjavaexample.network.ShutterstockService;
import pl.itako.rxjavaexample.network.api.Page;
import rx.Observable;

public class IdlingApiServiceWrapper implements ShutterstockService, IdlingResource {

    private final ShutterstockService api;
    private final AtomicInteger counter;
    private final List<ResourceCallback> callbacks;

    public IdlingApiServiceWrapper(ShutterstockService api) {
        this.api = api;
        this.callbacks = new ArrayList<>();
        this.counter = new AtomicInteger(0);
    }

    @Override
    public String getName() {
        return this.getClass().getName();
    }

    @Override
    public boolean isIdleNow() {
        return counter.get() == 0;
    }

    @Override
    public void registerIdleTransitionCallback(ResourceCallback resourceCallback) {
        callbacks.add(resourceCallback);
    }

    private void notifyIdle() {
        if (counter.get() == 0) {
            for (ResourceCallback cb : callbacks) {
                cb.onTransitionToIdle();
            }
        }
    }

    @Override
    public Observable<Page> getPage(int page) {
        counter.incrementAndGet();
        return api.getPage(page).finallyDo(() -> {
            counter.decrementAndGet();
            notifyIdle();
        });
    }

}
